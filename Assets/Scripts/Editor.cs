﻿using UnityEngine;

//using UnityEditor;
using System.IO;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;


public class Editor : MonoBehaviour
{

    public bool showDialog = false;
    public string img_path;
    public string json_path;
    public string dialog_path;
    public string variant_path;
    public string msg_font_path;
    public GameObject level_num;
    public GameObject background;
    public GameObject add_variant;
    public GameObject variant;
    public GameObject canvas;
    public GameObject msg;
    public GameObject remover;
    public GameObject nxt_player_img;
    public float variant_pos_y = 100f;
    public Text msg_txt;
    public bool scene_is_ready = false;
    public bool dlt_is_created = false;
    public List<GameObject> variants;
    public GameObject dlt;
    public bool is_delete = false;
    public GameObject saved;
    public GameObject not_saved;


    void Start()
    {
        variants = new List<GameObject>();
    }
    public void SelectBackground()
    {
        var openFile = new System.Windows.Forms.OpenFileDialog();
        openFile.InitialDirectory = Application.dataPath + "/Resources/BackImages";
        string path = openFile.InitialDirectory;
        openFile.ShowDialog();
        img_path = openFile.FileName;
        if (img_path.Length != 0 && (img_path.EndsWith(".png") || img_path.EndsWith(".jpg")))
        {
            var bytes = File.ReadAllBytes(img_path);
            var tex = new Texture2D(1, 1);
            tex.LoadImage(bytes);
            background.GetComponent<Renderer>().material.mainTexture = tex;
        }
        img_path = openFile.FileName.Replace(path + "\\", "");
    }


    public void Save()
    {

        string level_num = this.level_num.GetComponent<GetText>().GetTxt().Trim();
        if (level_num.Length > 0)
        {
            GameObject[] variants = GameObject.FindGameObjectsWithTag("Variant");
            GameObject[] player_images = GameObject.FindGameObjectsWithTag("Player");
            var json = JsonUtility.ToJson(new JsonObjects(level_num, img_path, new Stats(0, 0, 0, 0, 0), msg, msg_font_path, variants, player_images));
            SaveItemInfo(json);
            saved.SetActive(true);
        }
        else 
            not_saved.SetActive(true);
    }

    public void AddMessage()
    {
        if (!msg.activeInHierarchy)
            msg.SetActive(true);
    }

    public void SelectFont()
    {
        var openFile = new System.Windows.Forms.OpenFileDialog();
        openFile.InitialDirectory = Application.dataPath + "/Resources/Fonts";
        string msg_font_path = openFile.InitialDirectory;
        openFile.ShowDialog();
        this.msg_font_path = openFile.FileName;
        this.msg_font_path = openFile.FileName.Replace(msg_font_path + "\\", "");
        if (this.msg_font_path.Length != 0 && (this.msg_font_path.EndsWith(".ttf") || this.msg_font_path.EndsWith(".otf")))
        {
            if (this.msg_font_path.EndsWith(".ttf"))
                this.msg_font_path = this.msg_font_path.Replace(".ttf", "");
            else
                this.msg_font_path = this.msg_font_path.Replace(".otf", "");
            Font font = (Font)Resources.Load("Fonts/" + this.msg_font_path);
            //Font font = (Font)Resources.GetBuiltinResource (typeof(Font), this.msg_font_path);
            msg_txt.font = font;

        }

    }

    public void AddVariant()
    {
        if (variants.Count != 7)
        {
            if (variants.Count == 0)
            {
                GameObject obj = (GameObject)Instantiate(variant, new Vector2(add_variant.transform.position.x, add_variant.transform.position.y - variant_pos_y), Quaternion.identity, canvas.transform);
                variants.Add(obj);
            }
            else
            {
                GameObject obj = (GameObject)Instantiate(variant, new Vector2(add_variant.transform.position.x, variants[variants.Count - 1].transform.position.y - variant_pos_y), Quaternion.identity, canvas.transform);
                variants.Add(obj);
            }
        }
    }


    public void SelectPlayer(GameObject player)
    {
        int count_player_images = GameObject.FindGameObjectsWithTag("Player").Length;

        if (count_player_images < 5)
        {

            var openFile = new System.Windows.Forms.OpenFileDialog();
            openFile.InitialDirectory = Application.dataPath + "/Resources/PlayerImages";
            string path = openFile.InitialDirectory;
            openFile.ShowDialog();
            var img_path = openFile.FileName;
            if (img_path.Length != 0 && (img_path.EndsWith(".png") || img_path.EndsWith(".jpg")))
            {
                var bytes = File.ReadAllBytes(img_path);
                var tex = new Texture2D(1, 1);
                tex.LoadImage(bytes);
                var sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));
                player.GetComponent<Image>().sprite = sprite;
                player.GetComponentInChildren<Text>().text = "";
                GameObject nxt_player = player.GetComponent<PlayerImage>().nxt_player;
                player.GetComponent<PlayerImage>().is_empty = false;
                if (!nxt_player)
                    player.GetComponent<PlayerImage>().nxt_player = Instantiate(nxt_player_img, new Vector2(player.transform.position.x, player.transform.position.y - 120f), Quaternion.identity,
                        canvas.transform) as GameObject;
            }
            player.GetComponent<PlayerImage>().img_path = openFile.FileName.Replace(path + "\\", "");
        }
    }


    public void SaveItemInfo(string str)
    {
        json_path = "LevelInfo.json";
        using (StreamWriter file =
                   new StreamWriter(json_path, true))
        {
            file.WriteLine(str);
        }
    }

    public void CreateRemover()
    {
        if (dlt_is_created == false)
            dlt = (GameObject)Instantiate(remover, remover.transform.position, Quaternion.identity, canvas.transform);
    }

    void Update()
    {
    }

}