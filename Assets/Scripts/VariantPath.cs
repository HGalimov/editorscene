﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class VariantPath
{
	public string variant = "";

	public VariantPath ()
	{
		GameObject[] variants = GameObject.FindGameObjectsWithTag ("variant");
		if (variants.Length > 0) {			
			for (int i = 0; i < variants.Length; i++) {								 
				variant += "v" + (i + 1) + "" + variants [i].GetComponentInChildren<Text> ().GetComponent<Variant> ().variant_path;

			}
		}         
	}
}
