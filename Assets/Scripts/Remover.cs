﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;

public class Remover : MonoBehaviour
{
    private List<RaycastResult> eventData;
    private bool resultAppendList;
    public bool dlt_is_created;
    public bool obj_is_deleted = false;
    public GameObject level;

    // Use this for initialization
    void Start()
    {
        level = GameObject.Find("_level");
        dlt_is_created = level.GetComponent<Editor>().dlt_is_created = true;
    }

    // Update is called once per frame
    void Update()
    {
        var v3 = Input.mousePosition;
        /*v3.z = 10.0f;
        v3 = Camera.main.ScreenToWorldPoint(v3);
        transform.position = new Vector3(v3.x, v3.y, -1);*/
        transform.position = Vector2.Lerp(transform.position, v3, 1);
        if (Input.GetMouseButton(1))
        {
            GameObject.Find("_level").GetComponent<Editor>().dlt_is_created = false;
            Destroy(gameObject);
        }

       

    }    

    public void Remove()
    {

        List<GameObject> variants = level.GetComponent<Editor>().variants;
        GameObject canvas = GameObject.Find("Canvas");
        GraphicRaycaster gr = canvas.GetComponent<GraphicRaycaster>();
        PointerEventData ped = new PointerEventData(null);
        ped.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        gr.Raycast(ped, results);

        if (results.Count > 0)
        {
            GameObject obj = results[0].gameObject.transform.parent.gameObject;
            string tag = obj.tag;
            if (tag.Equals("Variant"))
            {

                obj_is_deleted = true;
                var pos = obj.transform.position;                
                for (int i = variants.IndexOf(obj); i < variants.Count; i++)
                {
                    var cur_pos = variants[i].transform.position;
                    variants[i].transform.position = pos;
                    pos = cur_pos;
                }
                variants.Remove(obj);
                Destroy(obj);

            }

            if (tag.Equals("Player") && obj.GetComponent<PlayerImage>().is_empty != true)
            {
                List <GameObject> player_images = GameObject.FindGameObjectsWithTag("Player").ToList<GameObject>();
                var pos = obj.transform.position;
                for (int i = player_images.IndexOf(obj); i < player_images.Count; i++)
                {
                    var cur_pos = player_images[i].transform.position;
                    player_images[i].transform.position = pos;
                    pos = cur_pos;
                }
                Destroy(obj);
            }

            if (tag.Equals("Msg"))
                obj.SetActive(false);
        }
    }
}
