﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

//[System.Serializable]
public class LevelNum: MonoBehaviour{
	public int level_num;
	//[NonSerializedAttribute]
	public GameObject inp_level_num;
	public LevelNum()
	{
		string inp_level_num = this.inp_level_num.GetComponent<GetText>().txt;
		if (inp_level_num.Length != 0)
		level_num = Int32.Parse (inp_level_num);
	}
}
