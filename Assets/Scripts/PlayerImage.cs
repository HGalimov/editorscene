﻿using UnityEngine;
using System.Collections;
//using UnityEditor;
using System.IO;
using System;
using UnityEngine.UI;

public class PlayerImage : MonoBehaviour
{

	public string img_path;
	public GameObject player;
	public GameObject canvas;
	public GameObject level;
	public GameObject nxt_player;
    public bool is_empty = true;


	void Start () {
		canvas = GameObject.Find ("Canvas");
		level = GameObject.Find ("_level");
	}
	public void SelectPlayer () {
		level.GetComponent<Editor> ().SelectPlayer (player);
	}
}
