﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class Message: MonoBehaviour{
	public string txt;
	public Message(GameObject text)
	{
		
		txt = text.GetComponent<Text>().text;
	}
}
