﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Stats
{

    public int force;
    public int intellect;
    public int charisma;
    public int karma;
    public int mana;

    public Stats(int force, int intellect, int charisma, int karma, int mana)
    {
        this.force = force;
        this.intellect = intellect;
        this.charisma = charisma;
        this.karma = karma;
        this.mana = mana;
    }

}
