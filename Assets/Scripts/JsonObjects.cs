﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[System.Serializable]
public class JsonObjects
{
	public string level_num;
	public string img_path;
	public int force;
	public int intellect;
	public int charisma;
	public int karma;
	public int mana;
	public string msg;
	public string msg_font_path;
	public string variants = "";
	public string player_images = "";


	public JsonObjects (String level_num, String img_path, Stats stats, GameObject msg, string msg_font_path, GameObject[] variants, GameObject[] player_images)
	{
		this.level_num = level_num;
		this.img_path = img_path;
		this.force = stats.force;
		this.intellect = stats.intellect;
		this.charisma = stats.charisma;
		this.karma = stats.karma;
		this.mana = stats.mana;
		this.msg = msg.GetComponent<GetText> ().GetTxt ().Trim ();
		this.msg_font_path = msg_font_path;
		foreach (var variant in variants) {
            if (variant.GetComponent<GetText>().GetTxt().Length != 0)
            {
                if (this.variants.Length == 0)
                    this.variants += variant.GetComponent<GetText>().GetTxt().Trim();
                else
                    this.variants += "||" + variant.GetComponent<GetText>().GetTxt().Trim();
            }
		}
		foreach (var variant in player_images) {
			if (variant.GetComponent<PlayerImage> ().img_path.Length != 0) {
				if (this.player_images.Length == 0)
					this.player_images += variant.GetComponent<PlayerImage> ().img_path;
				else
					this.player_images += "||" + variant.GetComponent<PlayerImage> ().img_path;				
			}
		}
	}
}
